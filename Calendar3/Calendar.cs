﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Calendar3
{
    public partial class txtCalendarBox : Form
    {
        private MonthCalendar monthCalendar;
        //private TextBox txtCalendarInnerBox;
  
    public txtCalendarBox()
    {
        InitializeComponent();

        txtCalInnerBox.ReadOnly = true;

        // Create the calendar. 
        monthCalendar = new MonthCalendar();
       
        // Set the calendar location. 
        this.monthCalendar.Location = new Point(200, 30);

        // Change the color. 
        monthCalendar.BackColor = System.Drawing.SystemColors.Info;
        monthCalendar.ForeColor = System.Drawing.Color.Black;
        monthCalendar.TitleBackColor = System.Drawing.Color.SteelBlue;
        monthCalendar.TitleForeColor = System.Drawing.Color.Black;
        monthCalendar.TrailingForeColor = System.Drawing.Color.Black;

         //Add dates to the AnnuallyBoldedDates array. 
            monthCalendar.AnnuallyBoldedDates = 
            new System.DateTime[] { new System.DateTime(2002, 4, 20, 0, 0, 0, 0),
                                    new System.DateTime(2002, 4, 28, 0, 0, 0, 0),
                                    new System.DateTime(2002, 5, 5, 0, 0, 0, 0),
                                    new System.DateTime(2002, 7, 4, 0, 0, 0, 0),
                                    new System.DateTime(2002, 12, 15, 0, 0, 0, 0),
                                    new System.DateTime(2002, 12, 18, 0, 0, 0, 0)};

         //Add dates to BoldedDates array. 
            monthCalendar.BoldedDates = new System.DateTime[] {new System.DateTime(2002, 9, 26, 0, 0, 0, 0)};

         //Add dates to MonthlyBoldedDates array. 
            monthCalendar.MonthlyBoldedDates = 
           new System.DateTime[] {new System.DateTime(2002, 1, 15, 0, 0, 0, 0),
                                  new System.DateTime(2002, 1, 30, 0, 0, 0, 0)};

        //Set Calendar Properties
        monthCalendar.CalendarDimensions = new Size(3, 1);
        monthCalendar.FirstDayOfWeek = Day.Sunday;
        monthCalendar.MaxDate = new DateTime(2020, 12, 31, 0, 0, 0, 0);
        monthCalendar.MinDate = new DateTime(1999, 1, 1, 0, 0, 0, 0);
        monthCalendar.MaxSelectionCount = 10;
        monthCalendar.ScrollChange = 1;
        monthCalendar.ShowToday = true;

        // Add event handlers for the DateSelected and DateChanged events 
        monthCalendar.DateSelected += new DateRangeEventHandler(monthCalendar1_DateSelected);
        monthCalendar.DateChanged += new DateRangeEventHandler(monthCalendar1_DateChanged);

        // Set up how the form should be displayed and add the controls to the form. 
        ClientSize = new Size(920, 566);
        Controls.AddRange(new Control[] {txtCalInnerBox, monthCalendar});
    }

    private void monthCalendar1_DateSelected(object sender, System.Windows.Forms.DateRangeEventArgs e)
    {
        // Show the start and end dates in the text box. 
        txtCalInnerBox.Text = "Date Selected: Start = " +
            e.Start.ToShortDateString() + " : End = " + e.End.ToShortDateString();
    }

    private void monthCalendar1_DateChanged(object sender, System.Windows.Forms.DateRangeEventArgs e)
    {
        // Show the start and end dates in the text box. 
        txtCalInnerBox.Text = "Date Changed: Start =  " +
            e.Start.ToShortDateString() + " : End = " + e.End.ToShortDateString();
    }
  }
}
