﻿namespace Calendar3
{
    partial class txtCalendarBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCalInnerBox = new System.Windows.Forms.TextBox();
            this.btnCreateReport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtCalInnerBox
            // 
            this.txtCalInnerBox.BackColor = System.Drawing.Color.LemonChiffon;
            this.txtCalInnerBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtCalInnerBox.Location = new System.Drawing.Point(0, 270);
            this.txtCalInnerBox.Multiline = true;
            this.txtCalInnerBox.Name = "txtCalInnerBox";
            this.txtCalInnerBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCalInnerBox.Size = new System.Drawing.Size(784, 285);
            this.txtCalInnerBox.TabIndex = 0;
            // 
            // btnCreateReport
            // 
            this.btnCreateReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateReport.Location = new System.Drawing.Point(615, 216);
            this.btnCreateReport.Name = "btnCreateReport";
            this.btnCreateReport.Size = new System.Drawing.Size(157, 39);
            this.btnCreateReport.TabIndex = 1;
            this.btnCreateReport.Text = "Create Report";
            this.btnCreateReport.UseVisualStyleBackColor = true;
            // 
            // txtCalendarBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 555);
            this.Controls.Add(this.btnCreateReport);
            this.Controls.Add(this.txtCalInnerBox);
            this.Name = "txtCalendarBox";
            this.Text = "Calendar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCalInnerBox;
        private System.Windows.Forms.Button btnCreateReport;

    }
}

